package com.example.amad.mypsychiatrist;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class DepVideos extends AppCompatActivity {

    Toolbar mToolbar;
    WebView webView;
    TextView txt;
    String YouTubeVideoEmbedCode1 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/EsSgRfRbpw0\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode2 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/7eGIgRco-OU\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode3 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/81BsbqxRETw\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode4 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/AZAu9e0LVjg\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode5 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/ASLO8XF8CeQ\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode6 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/TvF8MiXOb5U&t\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode7 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/zXtlTNvMFRU&list\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode8 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/ckoKiFfMe3k&list\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode9 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/FMtPIHKZ2y8\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode10 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/XJuoAIljn1M\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dep_videos);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        webView = (WebView) findViewById(R.id.webView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar8);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString("CountryName"));
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Saves You from Depression")) {
                WebView();


            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Make satisfy your mind")) {
                WebView1();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Irresistible way to release depression")) {
                WebView2();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Cure by Amazing Art")) {
                WebView4();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Satisfying Way for Relaxation")) {
                WebView5();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Amazing and Peaceful inventions")) {
                WebView6();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Ways to Cure Depression")) {
                WebView7();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Foods to Fight Depression")) {
                WebView8();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Natural Remedies for Depression")) {
                WebView9();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Dua to Remove Depression")) {
                WebView10();

            }


        }

    }

    private void WebView2() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode3, "text/html", "utf-8");
    }

    private void WebView1() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode2, "text/html", "utf-8");
    }

    public void WebView() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode1, "text/html", "utf-8");
    }


    private void WebView4() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode4, "text/html", "utf-8");
    }



    private void WebView5() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode5, "text/html", "utf-8");
    }

    private void WebView6() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode6, "text/html", "utf-8");
    }

    private void WebView7() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode7, "text/html", "utf-8");
    }


    private void WebView8() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode8, "text/html", "utf-8");
    }

    private void WebView9() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode9, "text/html", "utf-8");
    }

    private void WebView10() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode10, "text/html", "utf-8");
    }




}