package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class psychiatristLogin extends AppCompatActivity {

    EditText psyName;
    EditText psyPassword;
    TextView incTextView;
    String token;
    FirebaseDatabase reference;
    public void psyLogin(View view){

        String pName = psyName.getText().toString();
        String pPassword = psyPassword.getText().toString();

        if ((pName.equals("admin")) && (pPassword.equals("Adminn12345678"))) {
            token = FirebaseInstanceId.getInstance().getToken();
            HashMap<String , String> map = new HashMap<>();
            map.put("token", token);

             FirebaseDatabase.getInstance().getReference().child("admin").setValue(map);



           Intent intent = new Intent(psychiatristLogin.this , psychiatristHomePage.class);
           startActivity(intent);
        } else {
            Snackbar.make(view, "              Incorrect Username Or Password", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psychiatrist_login);

        psyName = findViewById(R.id.psyName);
        psyPassword = findViewById(R.id.psyPassord);
       // incTextView = findViewById(R.id.incTextView);

    }
}
