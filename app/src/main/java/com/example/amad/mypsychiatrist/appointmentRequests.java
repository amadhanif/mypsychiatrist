package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

public class appointmentRequests extends AppCompatActivity {

    ListView appiontmentsListView;
    FirebaseListAdapter adapter;
    ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment_requests);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Appointment Requests");
        appiontmentsListView =  findViewById(R.id.appiontmentsListView);
        arrayList = new  ArrayList<String>();

        Query query = FirebaseDatabase.getInstance().getReference().child("request");
        FirebaseListOptions<patientsList> options = new FirebaseListOptions.Builder<patientsList>()
                .setLayout(R.layout.patients_list)
                .setQuery(query, patientsList.class)
                .build();
        adapter = new FirebaseListAdapter(options) {
            @Override
            protected void populateView(View v, Object model, int position) {
                TextView ptName = v.findViewById(R.id.ptName);
                TextView ptAge = v.findViewById(R.id.ptAge);
                TextView ptGender = v.findViewById(R.id.ptGender);

                patientsList patientsList = (patientsList) model;

               arrayList.add(patientsList.getUid());
                ptName.setText(patientsList.getName().toString());
                ptAge.setText("Age: "+patientsList.getAge());
                ptGender.setText("Gender: "+patientsList.getGender().toString());

            }
        };
        appiontmentsListView.setAdapter(adapter);

        appiontmentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                // Toast.makeText(appointmentRequests.this, arrayList.get(i), Toast.LENGTH_SHORT).show();
                Intent pIntent = new Intent(appointmentRequests.this , sendSchedule.class);
                pIntent.putExtra("pid" , arrayList.get(i));


                startActivity(pIntent);
            }
        });

    }


    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
