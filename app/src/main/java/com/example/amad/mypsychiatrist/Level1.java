package com.example.amad.mypsychiatrist;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;
import com.google.firebase.database.ValueEventListener;

import java.util.logging.Level;

public class Level1 extends AppCompatActivity {

    DatabaseReference reference, choiceReference;
    String ch0, ch1, ch2, ch3, ch4, ch5;
    TextView questionsTextView;
    Button nextButton;
    RadioGroup radioGroup1;
    RadioButton choice0, choice1, choice2, choice3, choice4, choice5;
    int count = 0;
    public void onRadioButtonClicked(View view){

        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
         /*   case R.id.choice1:
                count = 5;
                break;
            case R.id.choice2:
                if (checked)
                    count = 4;
                break;
            case R.id.choice3:
                if (checked)
                    count = 3;
                break;
            case R.id.choice4:
                if (checked)
                    count = 2;
                break;
            case R.id.choice5:
                if (checked)
                    count = 1;
                break;*/

            case R.id.choice1:
                if (checked)
                count = 0;
                break;
            case R.id.choice2:
                if (checked)
                    count = 1;
                break;
            case R.id.choice3:
                if (checked)
                    count = 2;
                break;
            case R.id.choice4:
                if (checked)
                    count = 3;
                break;

        }


   /*    int checkedId = radioGroup1.getCheckedRadioButtonId();
        Toast.makeText(this, Integer.toString(checkedId), Toast.LENGTH_LONG).show();*/

    }
    public void countWeight(View view) {

        if (TextUtils.isEmpty(ch0) && TextUtils.isEmpty(ch1) && TextUtils.isEmpty(ch2) && TextUtils.isEmpty(ch3)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Level1.this);

            builder.setTitle("Data Loading Alert");
            builder.setMessage("Please wait for data loading and connect your mobile with internet");
            builder.show();
        } else {

            //Toast.makeText(Level1.this, ""+count, Toast.LENGTH_SHORT).show();

            if (radioGroup1.getCheckedRadioButtonId() == -1) {

                Toast.makeText(this, "Please select an option", Toast.LENGTH_LONG).show();
            } else {

                Intent q1Intent = new Intent(Level1.this, Leve1Question2.class);

                q1Intent.putExtra("w1", count);
                startActivity(q1Intent);
            }

        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Level1.this , Main2Activity.class);
        startActivity(intent);
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1);
        questionsTextView= findViewById(R.id.QuestionsTextView);
        nextButton = findViewById(R.id.nextButton);
        radioGroup1= findViewById(R.id.radioGroup1);
        choice0= findViewById(R.id.choice1);
        choice1= findViewById(R.id.choice2);
        choice2= findViewById(R.id.choice3);
        choice3= findViewById(R.id.choice4);
        //choice5= findViewById(R.id.choice5);

        reference= FirebaseDatabase.getInstance().getReference().child("Questions").child("Level1").child("Question1");

        choiceReference= FirebaseDatabase.getInstance().getReference().child("Answers").child("Level1").child("Answer1");



        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String q1= dataSnapshot.child("Text").getValue().toString();
                questionsTextView.setText(q1);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        choiceReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ch0 =dataSnapshot.child("Choice0").getValue().toString();
                ch1 =dataSnapshot.child("Choice1").getValue().toString();
                ch2 =dataSnapshot.child("Choice2").getValue().toString();
                ch3 =dataSnapshot.child("Choice3").getValue().toString();
                /*ch4 =dataSnapshot.child("Choice4").child("Text").getValue().toString();
                ch5 =dataSnapshot.child("Choice5").child("Text").getValue().toString();*/
                choice0.setText(ch0);
                choice1.setText(ch1);
                choice2.setText(ch2);
                choice3.setText(ch3);
                /*choice4.setText(ch4);
                choice5.setText(ch5);*/


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });






    }
}
