package com.example.amad.mypsychiatrist;

import android.app.NotificationManager;
import android.app.Service;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String titlze= "E-Psychologist";
        String message=remoteMessage.getNotification().getBody();

        NotificationCompat.Builder builder= new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(titlze)
                .setContentText(message);

        NotificationManager notification= (NotificationManager) getSystemService(NOTIFICATION_SERVICE);


        int mId = ((int) System.currentTimeMillis());

        notification.notify(mId, builder.build());



    }

}
