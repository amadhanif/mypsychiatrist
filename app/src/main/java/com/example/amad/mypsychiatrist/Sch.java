package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Sch extends AppCompatActivity {

    Toolbar toolbar;
    int[] images = {R.drawable.sch1,R.drawable.sch2,R.drawable.sch3,R.drawable.sch4,R.drawable.sch5,R.drawable.sch6,R.drawable.sch7,R.drawable.sch8,R.drawable.sch9,R.drawable.sch10};
    String[] lessons = {"Cure for Schizophrenia Hypnosis", "Cure to Schizophrenia", "Schizophrenia – Introduction in Urdu", "Healing Code for Schizophrenia","Treat Schizophrenia Subliminal Extremely Powerful and Very Fast Results", "Music Therapy for Schizophrenia Hypnosis", "Get Rid of Schizophrenia Subliminal", "Schizophrenia – Musical Simulation","Higher Self voice – Schizophrenia Healing", "Natural Cure for Schizophrenia"};
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sch);
        list = (ListView) findViewById(R.id.list);
        toolbar = (Toolbar) findViewById(R.id.toolbar7);
        toolbar.setTitle("Videos");
        CustomArrayAdapter customlist = new CustomArrayAdapter(this, lessons, images);
        list.setAdapter(customlist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long pos=adapterView.getItemIdAtPosition(i);


                    String title=String.valueOf(adapterView.getItemAtPosition(i));
                    if (pos==0){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Rg8Hcs4uX5w")));
                    }
                    else if(pos==1){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=aZqbaA-qzUU")));
                    }

                    else if(pos==2){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=AlkbK9viSsc")));
                    }


                    else if(pos==3){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=ybf5ov2qBVA")));
                    }


                    else if(pos==4){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=nZhX_vGDXJc")));
                    }
                    else if(pos==5){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=jbfP6j5JBh0")));
                    }
                    else if(pos==6){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=KAgUUFjPp2w")));
                    }
                    else if(pos==7){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Zyh-1Y-0rzU")));
                    }
                    else if(pos==8){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=fuCSegWKfQA")));
                    }
                    else if(pos==9 ){
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=8eC8xAJR0p8")));
                    }

                }
        });


    }


    }


