package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

public class psychologistMessageBox extends AppCompatActivity {

    LinearLayout layoutPsy;
    RelativeLayout layout_2Psy;
    ImageView sendMsgButton;
    EditText messageArea;
    ScrollView scrollViewPsy;
    FirebaseAuth mAuth;
    DatabaseReference reference, ptReference;
    String patName, patId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_psychologist_message_box);
        Intent patIntent= getIntent();
         patId=  patIntent.getStringExtra("patId");
        patName=  patIntent.getStringExtra("patName");

        layoutPsy = findViewById(R.id.layoutPsy);
        layout_2Psy = findViewById(R.id.layout2Psy);
        sendMsgButton = findViewById(R.id.sendMsgButton);
        messageArea = findViewById(R.id.messageArea);
        scrollViewPsy = findViewById(R.id.scrollViewPsy);
        Toast.makeText(this, patId, Toast.LENGTH_SHORT).show();
        ptReference = FirebaseDatabase.getInstance().getReference().child("Users").child(patId).child("Messages").child("PatMessages");
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child(patId).child("Messages");
        HashMap<String , String> hashMap = new HashMap<String, String>();
        hashMap.put("PsyMessages", "0");
        reference.updateChildren(new HashMap(hashMap));
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child(patId).child("Messages").child("PsyMessages");
       HashMap<String , String> stringHashMap = new HashMap<String, String>();
        stringHashMap.put("message", "0");
        reference.push().setValue(stringHashMap);

       /* HashMap<String , String> hMap = new HashMap<String, String>();
        hMap.put("message", "0");
        ptReference.updateChildren(new HashMap(hMap));*/
        sendMsgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();

                if (!messageText.equals("")) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("message", messageText);
                    reference.push().setValue(map);
                    messageArea.setText("");
                }
            }
        });
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    String message = ds.child("message").getValue(String.class).toString();
                    addMessageBox("Psychologist" + ":-\n" + message, 2);
                }
              /*  String message = dataSnapshot.child("message").getValue().toString();
                addMessageBox("Psychologist" + ":-\n" + message, 2);*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        ptReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    String message = ds.child("message").getValue(String.class).toString();
                    addMessageBox( patName + ":-\n" + message, 1);

                }
               // String message = dataSnapshot.child("message").getValue().toString();
               // String userName = dataSnapshot.child("user").getValue().toString();


             //   addMessageBox( patName + ":-\n" + message, 1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void addMessageBox(String message, int type) {
        TextView textView = new TextView(psychologistMessageBox.this);
        textView.setText(message);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;

        if (type == 1) {
            lp2.gravity = Gravity.START;
            textView.setBackgroundResource(R.drawable.bubble_in);
        } else {
            lp2.gravity = Gravity.END;
            textView.setBackgroundResource(R.drawable.bubble_out);
        }
        textView.setLayoutParams(lp2);
        layoutPsy.addView(textView);
        scrollViewPsy.fullScroll(View.FOCUS_DOWN);
    }

}