package com.example.amad.mypsychiatrist;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.provider.SelfDestructiveThread;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class patientTasks extends AppCompatActivity {
    FirebaseAuth mAuth;
    DatabaseReference reference, databaseReference;
    String formatedResult;
    String age;
    String gender;
    String name;
    String depressionResult;
    String ocdResult;
    String uId;
    String schizoResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_tasks);

         Intent depIntent = getIntent();
         depressionResult = depIntent.getStringExtra("DepressionResult");

        Intent ocdIntent = getIntent();
        ocdResult = ocdIntent.getStringExtra("ocdResult");

        Intent schIntent = getIntent();
        schizoResult = schIntent.getStringExtra("schizoResult");

         ActionBar actionBar = getSupportActionBar();
         actionBar.setTitle("Recommendations");

         mAuth = FirebaseAuth.getInstance();
         reference = FirebaseDatabase.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        mAuth=FirebaseAuth.getInstance();
        String current_user = mAuth.getCurrentUser().getUid();

        /* if (getSharedPreferences("ur",MODE_PRIVATE).getBoolean("sent",false)){
             sendApptBtn.setText("Your Request is in processing");
             sendApptBtn.setEnabled(false);
         }*/

        /* age = "22";
         gender = "male";
         name = "abc";*/
        uId = mAuth.getCurrentUser().getUid();
        reference.child("Users").child(uId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Toast.makeText(patientTasks.this, "hello "+ dataSnapshot.child("Name").getValue().toString(), Toast.LENGTH_SHORT).show();
                age = dataSnapshot.child("Age").getValue().toString();
                gender= dataSnapshot.child("Gender").getValue().toString();
                name=  dataSnapshot.child("Name").getValue().toString();
                //Toast.makeText(ShowProfile.this, "age"+ age + "gender" +gender + "name" +name + "phone_no" + phone_no , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }




    public void talk(View view) {

        Intent tintent = new Intent(patientTasks.this , newMessages.class);
        tintent.putExtra("msgK", 1);
        startActivity(tintent);

    }

    public void sendAppointmentRequest(View view) {


        HashMap<String , String> hashMap = new HashMap<String, String>();

        hashMap.put("Name", name);
        hashMap.put("Age", age);
        hashMap.put("Gender", gender);
        hashMap.put("uid", uId);
        String currentUser = mAuth.getCurrentUser().getUid();
       databaseReference= FirebaseDatabase.getInstance().getReference().child("request").child(currentUser);
       databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
           @Override
           public void onComplete(@NonNull Task<Void> task) {
               if (task.isSuccessful()){
                   Toast.makeText(patientTasks.this, "Appointment Request Sent", Toast.LENGTH_SHORT).show();
               }
               else {
                   Toast.makeText(patientTasks.this, "Appointment Request not Sent", Toast.LENGTH_SHORT).show();
               }
           }
       });


        /*SharedPreferences preferences = getSharedPreferences("ur" , MODE_PRIVATE);
        preferences.edit().putBoolean("sent" , true).apply();
        sendApptBtn.setText("You Request is in processing");
        sendApptBtn.setEnabled(false);*/
    }

    public void games(View view) {

        Intent intent = new Intent(patientTasks.this , emptyActivity.class);
        startActivity(intent);
    }

    public void goBack(View view) {

        Intent gbIntent = new Intent(patientTasks.this , Main2Activity.class);

        startActivity(gbIntent);
        finishAffinity();
    }

    public void videos(View view) {
        Intent vIntent = new Intent(patientTasks.this , videos.class);
        startActivity(vIntent);
    }
}
