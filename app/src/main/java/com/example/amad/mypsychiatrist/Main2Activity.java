package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RelativeLayout level1, level2, level3, level4;
    private FirebaseAuth mAuth;

    DatabaseReference reference;
    String appt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Home");
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Home");
        mAuth=FirebaseAuth.getInstance();
        String current_user = mAuth.getCurrentUser().getUid();

        reference= FirebaseDatabase.getInstance().getReference().child("Users").child(current_user);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
             appt = dataSnapshot.child("Schedule").getValue().toString();
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                if (appt == "0") {
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Snackbar.make(view, "No Schedule Found!!", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                    });
                }
                else {

                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Snackbar.make(view, "Your Appointment is Scheduled on "+ appt, Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                    });

                }

        }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar.make(findViewById(R.id.coordinator_layout_slide), "No schedule found!!" , Snackbar.LENGTH_LONG).show();


                    }
                });

            }
        });



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        level1 = findViewById(R.id.btnLevel1);
        level2 = findViewById(R.id.btnLevel2);
        level3 = findViewById(R.id.btnLevel3);
        level4 = findViewById(R.id.btnLevel4);


      /*  level1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Main2Activity.this , Level1.class);
                startActivity(intent);
            }
        });*/
        SharedPreferences levels = getSharedPreferences("myPreff", MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("myPreff", MODE_PRIVATE).edit();

        String level = levels.getString("level", "1");
        //last_pos = getSharedPreferences("myPreff",MODE_PRIVATE).getString("levels","1");
        //  Toast.makeText(this, level , Toast.LENGTH_SHORT).show();
        if (level.equals("1")) {
            level1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level1.class);
                    startActivity(in);
                }
            });
            level2.setBackgroundColor(getResources().getColor(R.color.gray));
            level3.setBackgroundColor(getResources().getColor(R.color.gray));
            level4.setBackgroundColor(getResources().getColor(R.color.gray));

        }

        if (level.equals("2")) {
            level1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level1.class);
                    startActivity(in);
                }
            });
            level2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level2Question1.class);
                    startActivity(in);
                }
            });
            level3.setBackgroundColor(getResources().getColor(R.color.gray));
            level4.setBackgroundColor(getResources().getColor(R.color.gray));
        }


        if (level.equals("3")) {
            level3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level3Question1.class);
                    startActivity(in);
                }
            });
            level1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level1.class);
                    startActivity(in);
                }
            });
            level2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level2Question1.class);
                    startActivity(in);
                }
            });
            level4.setBackgroundColor(getResources().getColor(R.color.gray));


        }

        if (level.equals("4")) {
            level3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level3Question1.class);
                    startActivity(in);
                }
            });
            level1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level1.class);
                    startActivity(in);
                }
            });
            level2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level2Question1.class);
                    startActivity(in);
                }
            });
            level4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(Main2Activity.this, Level4Question1.class);
                    startActivity(in);
                }
            });
        }

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);


        } else {

            getApplication().onTerminate();
            super.onBackPressed();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.reminder) {
            // Handle the reminder action
            Intent in=new Intent(Main2Activity.this,mainReminder.class);
            startActivity(in);
        } else if (id == R.id.about) {

            Intent ain=new Intent(Main2Activity.this,about.class);
            startActivity(ain);
        } else if (id == R.id.credits) {
            Intent cin=new Intent(Main2Activity.this,credits.class);
            startActivity(cin);
        } else if (id == R.id.signOut) {

            FirebaseAuth.getInstance().signOut();
            SharedPreferences sp = getSharedPreferences("myPref", MODE_PRIVATE);
            SharedPreferences.Editor spE = sp.edit();
            spE.putBoolean("reg", false);
            spE.commit();
            Intent soIntent = new Intent(Main2Activity.this , WelcomeActivity.class);
            startActivity(soIntent);

        } else if (id == R.id.pro) {

            Intent pIntent= new Intent(Main2Activity.this, ShowProfile.class);
            startActivity(pIntent);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;



    }


}
