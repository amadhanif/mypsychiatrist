package com.example.amad.mypsychiatrist;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class MessageBox extends AppCompatActivity {

    LinearLayout layout;
    RelativeLayout layout_2;
    ImageView sendMsgButton;
    EditText messageArea;
    ScrollView scrollView;
    FirebaseAuth mAuth;
    DatabaseReference reference, psyReference;
    String current_user, ptname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_box);
        layout = findViewById(R.id.layout1);
        layout_2 = findViewById(R.id.layout2);
        sendMsgButton = findViewById(R.id.sendMsgButton);
        messageArea = findViewById(R.id.messageArea);
        scrollView = findViewById(R.id.scrollView);
        mAuth = FirebaseAuth.getInstance();
        current_user = mAuth.getCurrentUser().getUid();
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Messages");
        psyReference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Messages").child("PsyMessages");
       /* psyReference.push().setValue(current_user);
        psyReference.child(current_user).push().setValue("Messages");*/
        HashMap<String , String> hashMap = new HashMap<String, String>();
        hashMap.put("PatMessages", "0");
        reference.updateChildren(new HashMap(hashMap));
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child(current_user).child("Messages").child("PatMessages");

        HashMap<String , String> hMap = new HashMap<String, String>();
        hMap.put("message","0");
        reference.updateChildren(new HashMap(hMap));


        HashMap<String , String> hhMap = new HashMap<String, String>();
        hhMap.put("message","0");
        reference.updateChildren(new HashMap(hhMap));

        SharedPreferences prefs = getSharedPreferences("ptNamePref", MODE_PRIVATE);
       /* String restoredText = prefs.getString("text",);
        if (restoredText != null) {*/
        ptname = prefs.getString("ptName", "No name defined");//"No name defined" is the default value.
    /*}*/


        sendMsgButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageArea.getText().toString();

                if (!messageText.equals("")) {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("message", messageText);
                    map.put("user", ptname);
                   reference.push().setValue(map);
                    /*psyReference.updateChildren(new HashMap(map)).addOnCompleteListener(new OnCompleteListener() {
                        @Override
                        public void onComplete(@NonNull Task task) {
                            if (task.isSuccessful()){
                                Toast.makeText(MessageBox.this, "msg send", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });*/
                    messageArea.setText("");
                }
            }
        });


        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()){
//                    String message = ds.child("message").getValue(String.class).toString();
                    addMessageBox("You:-\n" + "hello", 1);

                }

             /*   Map map = dataSnapshot.getValue(Map.class);*/

              //  String message = dataSnapshot.child("message").getValue(Map.class).toString();
                //String userName = dataSnapshot.child("user").getValue().toString();

               // if (userName.equals(ptname)) {
                   // addMessageBox("You:-\n" + message, 1);
              //  }
                /*else {
                    addMessageBox("Psychologist" + ":-\n" + message, 2);
                } */
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

               // Map map = dataSnapshot.getValue(Map.class);
                GenericTypeIndicator<Map<String, String>> genericTypeIndicator = new GenericTypeIndicator<Map<String, String>>() {};
               // Map<String, String> map = dataSnapshot.getValue(genericTypeIndicator );
                Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                //String userName = map.get("user").toString();

              *//*  if (userName.equals(ptname)) {*//*
                    addMessageBox("You:-\n" + message, 1);
             *//*   } else {
                    addMessageBox("Psychologist" + ":-\n" + message, 2);
                }*//*
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/
        psyReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
//                String message = dataSnapshot.child("message").getValue().toString();
                addMessageBox("Psychologist" + ":-\n" + "hello", 2);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void addMessageBox(String message, int type) {
        TextView textView = new TextView(MessageBox.this);
        textView.setText(message);

        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp2.weight = 1.0f;

        if (type == 1) {
            lp2.gravity = Gravity.START;
            textView.setBackgroundResource(R.drawable.bubble_in);
        } else {
            lp2.gravity = Gravity.END;
            textView.setBackgroundResource(R.drawable.bubble_out);
        }
        textView.setLayoutParams(lp2);
        layout.addView(textView);
        scrollView.fullScroll(View.FOCUS_DOWN);
    }
}