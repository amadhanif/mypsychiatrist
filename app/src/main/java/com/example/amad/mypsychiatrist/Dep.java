package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class Dep extends AppCompatActivity {

    Toolbar toolbar;
    int[] images = {R.drawable.depr1,R.drawable.drep2,R.drawable.depr3,R.drawable.depre4,R.drawable.depr5,R.drawable.depr6,R.drawable.depr7,R.drawable.depr8,R.drawable.depr9,R.drawable.depr10};
    String[] lessons = {"Saves You from Depression", "Make satisfy your mind", "Irresistible way to release depression", "Cure by Amazing Art","Satisfying Way for Relaxation", "Amazing and Peaceful inventions", "Ways to Cure Depression", "Foods to Fight Depression", "Natural Remedies for Depression", "Dua to Remove Depression"};
    ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dep);
        list = (ListView) findViewById(R.id.list);
        toolbar = (Toolbar) findViewById(R.id.toolbar7);
        toolbar.setTitle("Videos");
        CustomArrayAdapter customlist = new CustomArrayAdapter(this, lessons, images);
        list.setAdapter(customlist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long pos=adapterView.getItemIdAtPosition(i);


                String title=String.valueOf(adapterView.getItemAtPosition(i));
                if (pos==0){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=EsSgRfRbpw0")));
                }
                else if(pos==1){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=7eGIgRco-OU")));
                }

                else if(pos==2){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=81BsbqxRETw")));
                }


                else if(pos==3){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=AZAu9e0LVjg")));
                }


                else if(pos==4){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=ASLO8XF8CeQ")));
                }
                else if(pos==5){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=TvF8MiXOb5U&t=1s")));
                }
                else if(pos==6){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=zXtlTNvMFRU&list=RDCjCiyY2cGK8&index=22")));
                }
                else if(pos==7){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=ckoKiFfMe3k&list=RDCjCiyY2cGK8&index=23")));
                }
                else if(pos==8){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=FMtPIHKZ2y8")));
                }
                else if(pos==9 ){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=XJuoAIljn1M")));
                }

            }



        });


    }


}