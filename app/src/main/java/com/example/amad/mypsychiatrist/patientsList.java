package com.example.amad.mypsychiatrist;

public class patientsList {

    private String Gender, Name;
    private String Age , uid;

    public patientsList() {
    }

    public patientsList(String age, String gender, String name , String Uid) {

        Age = age;
        Gender = gender;
        Name = name;
        uid= Uid;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}

