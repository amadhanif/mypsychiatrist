package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Level4Question10 extends AppCompatActivity {

    DatabaseReference reference, choiceReference;
    String ch1, ch2, ch3, ch4, ch5;
    TextView questionsTextView;
    RadioGroup radioGroup1;
    RadioButton choice1, choice2, choice3, choice4, choice5;
    int w10 = 0;
    int totalw;

    public void onRadioButtonClicked(View view){

        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.choice1:
                w10 = 5;
                break;
            case R.id.choice2:
                if (checked)
                    w10 = 4;
                break;
            case R.id.choice3:
                if (checked)
                    w10 = 3;
                break;
            case R.id.choice4:
                if (checked)
                    w10 = 2;
                break;
            case R.id.choice5:
                if (checked)
                    w10 = 1;
                break;

        }


   /*    int checkedId = radioGroup1.getCheckedRadioButtonId();
        Toast.makeText(this, Integer.toString(checkedId), Toast.LENGTH_LONG).show();*/

    }
    public void countW2(View view){

        if (radioGroup1.getCheckedRadioButtonId() == -1){

            Toast.makeText(this, "Please select an option ", Toast.LENGTH_LONG).show();
        }

        else {

            totalw= w10 + totalw;

            //Toast.makeText(this, Integer.toString(totalw), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(Level4Question10.this, CalculateResult.class);

            intent.putExtra("totalw", totalw);
            startActivity(intent);

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level4_question10);

        Intent q1Intent = getIntent();
        totalw = q1Intent.getIntExtra("totalw", 0);

        questionsTextView= findViewById(R.id.QuestionsTextView);
        radioGroup1= findViewById(R.id.radioGroup1);
        choice1= findViewById(R.id.choice1);
        choice2= findViewById(R.id.choice2);
        choice3= findViewById(R.id.choice3);
        choice4= findViewById(R.id.choice4);
        choice5= findViewById(R.id.choice5);

        reference= FirebaseDatabase.getInstance().getReference().child("Questions").child("Level4").child("Question10");
        choiceReference= FirebaseDatabase.getInstance().getReference().child("Answers");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String q2= dataSnapshot.child("Text").getValue().toString();
                questionsTextView.setText(q2);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        choiceReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ch1 =dataSnapshot.child("Choice1").child("Text").getValue().toString();
                ch2 =dataSnapshot.child("Choice2").child("Text").getValue().toString();
                ch3 =dataSnapshot.child("Choice3").child("Text").getValue().toString();
                ch4 =dataSnapshot.child("Choice4").child("Text").getValue().toString();
                ch5 =dataSnapshot.child("Choice5").child("Text").getValue().toString();
                choice1.setText(ch1);
                choice2.setText(ch2);
                choice3.setText(ch3);
                choice4.setText(ch4);
                choice5.setText(ch5);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
