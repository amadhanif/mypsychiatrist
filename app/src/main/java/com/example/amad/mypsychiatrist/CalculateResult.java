package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class CalculateResult extends AppCompatActivity {

    ProgressBar resultProgressBar;
    TextView resultCtextView , resultTextView , scoreTextView;
    RelativeLayout resultLayout;
    int totalw;
    double div , result;
    FirebaseAuth mAuth;
    DatabaseReference reference;
    Button proFurther , sendR;
    TextView clickNextTextView;
    String formatedResult;
    public void calculateTheResult() {


        result = ((double) totalw / 42.0) * 200.0;
        /* NumberFormat nm = NumberFormat.getNumberInstance();*/
        //String finalResult = Double.toString(result);


       formatedResult =String.format("%.2f", result);
        resultTextView.setText("" + formatedResult + "%");
        /*  nm.setMaximumFractionDigits(2);*/
        // resultTextView.setText(nm.format(result) +"%");
        //Toast.makeText(CalculateResult.this,Float.toString(totalw) + "weight" , Toast.LENGTH_LONG).show();

        if (result < 50.0) {

            resultTextView.setTextColor(Color.parseColor("#00FF00"));
        } else if (result > 50.0 && result < 80.0) {

            resultTextView.setTextColor(Color.parseColor("#FFFF00"));
        } else {

            resultTextView.setTextColor(Color.parseColor("#FF0000"));
        }

       /* String currentUser = mAuth.getCurrentUser().getUid();

        reference =   FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser);



        Map<String, String> hashMap = new HashMap<>();

        hashMap.put("Anxiety Result", formatedResult);
        reference.updateChildren(new HashMap(hashMap));*/
    }

    public void sendResults(View view) {

        String currentUser = mAuth.getCurrentUser().getUid();

        reference =   FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser);


        SharedPreferences levels = getSharedPreferences("myPreff", MODE_PRIVATE);
        SharedPreferences.Editor editor = getSharedPreferences("myPreff", MODE_PRIVATE).edit();
        editor.putString("level","2");
        editor.commit();

        Map<String, String> hashMap = new HashMap<>();

        hashMap.put("Anxiety Result", formatedResult);

        reference.updateChildren(new HashMap(hashMap)).addOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {
                if (task.isSuccessful()){
                    Toast.makeText(CalculateResult.this, "Results are successfully sent to Psychologist", Toast.LENGTH_SHORT).show();

                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(CalculateResult.this, "There is some error in result sending" + e, Toast.LENGTH_SHORT).show();
            }
        });

        sendR.setVisibility(View.INVISIBLE);
        proFurther.setVisibility(View.VISIBLE);
        clickNextTextView.setText("Click Next");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_result);

        mAuth = FirebaseAuth.getInstance();

        reference= FirebaseDatabase.getInstance().getReference();
        Intent q1Intent = getIntent();
        totalw = q1Intent.getIntExtra("totalw", 0);

        resultProgressBar = findViewById(R.id.resultProgressBar);
        resultTextView= findViewById(R.id.resultTextView);
        resultLayout = findViewById(R.id.resultLayout);
        scoreTextView = findViewById(R.id.scoreTextView);
        clickNextTextView = findViewById(R.id.clickNextTextview);
        proFurther = findViewById(R.id.proFurther);
        sendR = findViewById(R.id.sendResult);

        if (totalw == 0 || totalw == 1 || totalw == 2 || totalw == 3 || totalw == 4 || totalw == 5 || totalw == 6 || totalw == 7){

            scoreTextView.setText("Normal");
            scoreTextView.setTextColor(Color.parseColor("#00FF00"));
        }
        else  if (totalw == 8 || totalw == 9 || totalw == 10){

            scoreTextView.setText("Borderline abnormal");
            scoreTextView.setTextColor(Color.parseColor("#FFFF00"));
        }

        else if (totalw == 11 || totalw == 12 || totalw == 13 || totalw == 14 || totalw == 15 || totalw == 16 || totalw == 17 || totalw == 18 || totalw == 19 || totalw == 20 || totalw == 21){

            scoreTextView.setText("      Abnormal (Case)");
            scoreTextView.setTextColor(Color.parseColor("#FF0000"));
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                resultProgressBar.setVisibility(View.GONE);
                resultTextView.setVisibility(View.VISIBLE);
                clickNextTextView.setVisibility(View.VISIBLE);
                sendR.setVisibility(View.VISIBLE);

                calculateTheResult();
            }

        }, 2000);


        proFurther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nIntent = new Intent(CalculateResult.this , patientTasks.class);
                startActivity(nIntent);

            }

        });
    }
}
