package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Level1Question7 extends AppCompatActivity {

    DatabaseReference reference, choiceReference;
    String ch0, ch1, ch2, ch3, ch4, ch5;
    TextView questionsTextView;
    RadioGroup radioGroup1;
    RadioButton choice0, choice1, choice2, choice3, choice4, choice5;
    int w7 = 0;
    int totalw;

    public void onRadioButtonClicked(View view){

        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
                   switch(view.getId()) {
             /*   case R.id.choice1:
                    w7 = 5;
                    break;
                case R.id.choice2:
                    if (checked)
                        w7 = 4;
                    break;
                case R.id.choice3:
                    if (checked)
                        w7 = 3;
                    break;
                case R.id.choice4:
                    if (checked)
                        w7 = 2;
                    break;
                case R.id.choice5:
                    if (checked)
                        w7 = 1;
                    break;
*/

                       case R.id.choice1:
                           if (checked)
                           w7 = 0;
                           break;
                       case R.id.choice2:
                           if (checked)
                               w7 = 1;
                           break;
                       case R.id.choice3:
                           if (checked)
                               w7 = 2;
                           break;
                       case R.id.choice4:
                           if (checked)
                               w7 = 3;
                           break;
        }


   /*    int checkedId = radioGroup1.getCheckedRadioButtonId();
        Toast.makeText(this, Integer.toString(checkedId), Toast.LENGTH_LONG).show();*/

    }
    public void countW2(View view){

        if (TextUtils.isEmpty(ch0) && TextUtils.isEmpty(ch1) && TextUtils.isEmpty(ch2) && TextUtils.isEmpty(ch3)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(Level1Question7.this);

            builder.setTitle("Data Loading Alert");
            builder.setMessage("Please wait for data loading and connect your mobile with internet");
            builder.show();
        } else {


            if (radioGroup1.getCheckedRadioButtonId() == -1){

            Toast.makeText(this, "Please select an option ", Toast.LENGTH_LONG).show();
        }

        else {

            totalw= w7 + totalw;

            //Intent intent = new Intent(Level1Question7.this, Level1Question8.class);

            //Toast.makeText(Level1Question7.this, Integer.toString(totalw), Toast.LENGTH_LONG).show();

           Intent intent = new Intent(Level1Question7.this , CalculateResult.class);
            intent.putExtra("totalw", totalw);
            startActivity(intent);

        }
    }
    }

    @Override
    public void onBackPressed() {
        totalw = totalw - w7;

        //Toast.makeText(Level1Question7.this, Integer.toString(totalw), Toast.LENGTH_LONG).show();
        Intent pQintent = new Intent(Level1Question7.this , Level1Question6.class);
        startActivity(pQintent);
        super.onBackPressed();
    }


    public void previousQuestion(View view){

            totalw = totalw - w7;

        //Toast.makeText(Level1Question7.this, Integer.toString(totalw), Toast.LENGTH_SHORT).show();
        Intent pQintent = new Intent(Level1Question7.this , Level1Question6.class);
        startActivity(pQintent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level1_question7);

        Intent q1Intent = getIntent();
        totalw = q1Intent.getIntExtra("totalw", 0);

        questionsTextView= findViewById(R.id.QuestionsTextView);
        radioGroup1= findViewById(R.id.radioGroup1);
        /*choice1= findViewById(R.id.choice1);
        choice2= findViewById(R.id.choice2);
        choice3= findViewById(R.id.choice3);
        choice4= findViewById(R.id.choice4);
        choice5= findViewById(R.id.choice5);
*/
        choice0= findViewById(R.id.choice1);
        choice1= findViewById(R.id.choice2);
        choice2= findViewById(R.id.choice3);
        choice3= findViewById(R.id.choice4);

        reference= FirebaseDatabase.getInstance().getReference().child("Questions").child("Level1").child("Question7");
        choiceReference= FirebaseDatabase.getInstance().getReference().child("Answers").child("Level1").child("Answer7");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String q2= dataSnapshot.child("Text").getValue().toString();
                questionsTextView.setText(q2);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        choiceReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /*ch1 =dataSnapshot.child("Choice1").child("Text").getValue().toString();
                ch2 =dataSnapshot.child("Choice2").child("Text").getValue().toString();
                ch3 =dataSnapshot.child("Choice3").child("Text").getValue().toString();
                ch4 =dataSnapshot.child("Choice4").child("Text").getValue().toString();
                ch5 =dataSnapshot.child("Choice5").child("Text").getValue().toString();
                choice1.setText(ch1);
                choice2.setText(ch2);
                choice3.setText(ch3);
                choice4.setText(ch4);
                choice5.setText(ch5);*/
                ch0 =dataSnapshot.child("Choice0").getValue().toString();
                ch1 =dataSnapshot.child("Choice1").getValue().toString();
                ch2 =dataSnapshot.child("Choice2").getValue().toString();
                ch3 =dataSnapshot.child("Choice3").getValue().toString();


                choice0.setText(ch0);
                choice1.setText(ch1);
                choice2.setText(ch2);
                choice3.setText(ch3);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
}
