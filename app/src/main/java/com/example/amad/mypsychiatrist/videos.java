package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class videos extends AppCompatActivity {
    Button anx, dep, sch, ocd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        anx = (Button) findViewById(R.id.button);
        dep = (Button) findViewById(R.id.button2);
        sch = (Button) findViewById(R.id.button3);
        ocd = (Button) findViewById(R.id.button4);

        anx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(videos.this,Anx.class);
                startActivity(intent);
            }
        });

        dep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(videos.this,Dep.class);
                startActivity(intent);
            }
        });

        sch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(videos.this,Sch.class);
                startActivity(intent);
            }
        });


        ocd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(videos.this,OCD.class);
                startActivity(intent);
            }
        });
    }
}
