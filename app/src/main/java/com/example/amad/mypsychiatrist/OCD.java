package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.amad.mypsychiatrist.R;

public class OCD extends AppCompatActivity {

    Toolbar toolbar;
    int[] images = {R.drawable.ocd1,R.drawable.ocd2,R.drawable.ocd3,R.drawable.ocd4,R.drawable.ocd5,R.drawable.ocd6,R.drawable.ocd7,R.drawable.ocd8,R.drawable.ocd9,R.drawable.ocd10};
    String[] lessons = {"OCD – Introduction in Urdu","Freedom from OCD - Binaural Beats Session - By Thomas Hall","Obsessive Compulsive Disorder (OCD) Frequency Healing with Affirmations/Subliminal/Energy", "Obsessive Compulsive Disorder Treatment Frequency: OCD Relief Binaural Beats Sound Therapy","Freedom from OCD - Classical Music Subliminal Session - By Thomas Hall", "Obsessive Compulsive Disorder - Rife Frequencies", "OCD Relief Release Unhealthy Mental and Physical Obsessions", "OCD Symptoms: Obsessive Compulsive Disorder Symptoms","Freedom from OCD - Waterfall Sounds Subliminal Session - By Thomas Hall", "Natural Cure for OCD"};
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ocd);
        list = (ListView) findViewById(R.id.list);
        toolbar = (Toolbar) findViewById(R.id.toolbar7);
        toolbar.setTitle("Videos");
        CustomArrayAdapter customlist = new CustomArrayAdapter(this, lessons, images);
        list.setAdapter(customlist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long pos=adapterView.getItemIdAtPosition(i);


                String title=String.valueOf(adapterView.getItemAtPosition(i));
                if (pos==0){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=0EQL7UDg86U")));
                }
                else if(pos==1){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=ysldCUdGgxs")));
                }

                else if(pos==2){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=TcGsGF_us3w")));
                }


                else if(pos==3){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Z6HSltxq0oo")));
                }


                else if(pos==4){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Po-Gmd9byOQ")));
                }
                else if(pos==5){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=pSxs3YQBqiU")));
                }
                else if(pos==6){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=g0-oU51tdW8")));
                }
                else if(pos==7){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=hV2DOXhZ9qM")));
                }
                else if(pos==8){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=yJz9VKKFLZg")));
                }
                else if(pos==9 ){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=Rmzz2bvFrtw")));
                }

            }


        });


    }


}