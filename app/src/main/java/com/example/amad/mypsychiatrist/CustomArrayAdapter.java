package com.example.amad.mypsychiatrist;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Saher on 8/19/2018.
 */

public class CustomArrayAdapter extends ArrayAdapter<String>
{
    String[] lessons;
    int[] images;
    private Activity context;
    public CustomArrayAdapter (Activity context, String[] lessons,int[] images) {
        super(context,R.layout.customlist, lessons);
        this.context=context;
        this.lessons=lessons;
        this.images=images;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //  return super.getView(position, convertView, parent);
        View r = convertView;
        ViewHolder viewHolder=null;
        if (r == null) {
            LayoutInflater layoutinflater=context.getLayoutInflater();
            r=layoutinflater.inflate(R.layout.customlist,null,true);
            viewHolder=new ViewHolder(r);
            r.setTag(viewHolder);
        }
        else {
            viewHolder=(ViewHolder)r.getTag();
        }
        viewHolder.txt.setText(lessons[position]);
        viewHolder.img.setImageResource(images[position]);
        return r;

    }
    class ViewHolder
    {
        TextView txt;
        ImageView img;
        ViewHolder(View v)
        {
            txt=(TextView)v.findViewById(R.id.SText);
            img=(ImageView)v.findViewById(R.id.SImage);
        }
    }


}