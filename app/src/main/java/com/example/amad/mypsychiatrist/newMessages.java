package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;
import java.util.HashMap;

public class newMessages extends AppCompatActivity {

    FirebaseAuth mAuth;
    String curreny_user;
    DatabaseReference reference;
    ListView msgLV;
    FirebaseListAdapter adapter;
    ArrayList<nMessages> msgList;
    ImageView sendMsgBtn;
    EditText msgArea;
    String a ="1";
    Query query;

    int x = 0;
String m, n;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_messages);


        Intent patIntent = getIntent();
        int j =patIntent.getIntExtra("msgK", 1);

        if (j == 1){
            m= "You:-";
            n = "Psychologist";
            mAuth= FirebaseAuth.getInstance();
            curreny_user= mAuth.getCurrentUser().getUid();
            reference= FirebaseDatabase.getInstance().getReference().child("Users").child(curreny_user).child("Messages");
            a= "1";
            query = reference;
        }else {

            m= "Patient:-";
            n = "You";
            String pId = getIntent().getStringExtra("patId");
            a= "0";
            reference= FirebaseDatabase.getInstance().getReference().child("Users").child(pId).child("Messages");
            query = reference;
        }

        msgLV= findViewById(R.id.msgsLayout);
        sendMsgBtn= findViewById(R.id.sendMsgButton);
        msgArea = findViewById(R.id.messageArea);

        sendMsgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String messageText = msgArea.getText().toString();

                if (!messageText.equals("")) {
                    String uKey= reference.push().getKey();
                    nMessages msg = new nMessages(messageText, uKey, a);

                    reference.child(uKey).setValue(msg);
                    msgArea.setText("");
                }
            }
        });

        FirebaseListOptions<nMessages> options = new FirebaseListOptions.Builder<nMessages>()
                .setLayout(R.layout.box_n_message)
                .setQuery(query, nMessages.class)
                .build();

        adapter = new FirebaseListAdapter(options) {
            @Override
            protected void populateView(View v, Object model, int position) {
                TextView ptName = v.findViewById(R.id.message);
                TextView sName = v.findViewById(R.id.sName);
                RelativeLayout mLayout = v.findViewById(R.id.messageLayout);

                nMessages nMsg = (nMessages) model;

                if (nMsg.getSender().equals("1")){

                    sName.setText(m);
                    mLayout.setBackgroundColor(Color.GREEN);
                }
                else {
                    sName.setText(n);
                    mLayout.setBackgroundColor(Color.BLUE);
                }

               // arrayList.add(patientsList.getUid());
                ptName.setText(nMsg.getMsg());

            }
        };
        msgLV.setAdapter(adapter);


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
