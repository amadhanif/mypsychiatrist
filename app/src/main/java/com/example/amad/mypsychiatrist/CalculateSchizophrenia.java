package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CalculateSchizophrenia extends AppCompatActivity {

    ProgressBar resultProgressBar;
    TextView resultCtextView , resultTextView , scoreTextView;
    RelativeLayout resultLayout;
    int totalw;
    double div , result;
    Button proFurther;
    TextView clickNextTextView;
     String formatedResult;

    public void calculateTheResult(){


        result = ((double) totalw /42.0 ) * 200.0 ;

        ;
        formatedResult = String.format("%.2f", result);
        resultTextView.setText("" + formatedResult + "%");

        if (result < 50.0 ){

            resultTextView.setTextColor(Color.parseColor("#00FF00"));
        }
        else  if (result > 50.0 && result < 80.0 ){

            resultTextView.setTextColor(Color.parseColor("#FFFF00"));
        }

        else {

            resultTextView.setTextColor(Color.parseColor("#FF0000"));
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate_schizophrenia);

        Intent q1Intent = getIntent();
        totalw = q1Intent.getIntExtra("totalw", 0);

        resultProgressBar = findViewById(R.id.resultProgressBar);
        resultTextView= findViewById(R.id.resultTextView);
        resultLayout = findViewById(R.id.resultLayout);
        scoreTextView = findViewById(R.id.scoreTextView);
        clickNextTextView = findViewById(R.id.clickNextTextview);
        proFurther = findViewById(R.id.proFurther);

        if (totalw == 0 || totalw == 1 || totalw == 2 || totalw == 3 || totalw == 4 || totalw == 5 || totalw == 6 || totalw == 7){

            scoreTextView.setText("Normal");
            scoreTextView.setTextColor(Color.parseColor("#00FF00"));
        }
        else  if (totalw == 8 || totalw == 9 || totalw == 10){

            scoreTextView.setText("Borderline abnormal");
            scoreTextView.setTextColor(Color.parseColor("#FFFF00"));
        }

        else if (totalw == 11 || totalw == 12 || totalw == 13 || totalw == 14 || totalw == 15 || totalw == 16 || totalw == 17 || totalw == 18 || totalw == 19 || totalw == 20 || totalw == 21){

            scoreTextView.setText("      Abnormal (Case)");
            scoreTextView.setTextColor(Color.parseColor("#FF0000"));
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                resultProgressBar.setVisibility(View.GONE);
                resultTextView.setVisibility(View.VISIBLE);
                clickNextTextView.setVisibility(View.VISIBLE);
                proFurther.setVisibility(View.VISIBLE);

                calculateTheResult();
            }

        }, 2000);

        proFurther.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nIntent = new Intent(CalculateSchizophrenia.this , patientTasks.class);
                nIntent.putExtra("schizoResult" , formatedResult);
                startActivity(nIntent);
            }
        });
    }
}
