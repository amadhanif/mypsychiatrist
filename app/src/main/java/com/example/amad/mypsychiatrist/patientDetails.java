package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class patientDetails extends AppCompatActivity {

    String ptId;
    String age, gender , name , anxietyResult, depressionResult, ocdResult, schizoResult;
    TextView proName, proAge, proGender, proAnx, proDep, proOcd, proSch;
    ProgressBar progressBar;

    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        proName = findViewById(R.id.proName);
        proAge = findViewById(R.id.proAge);
        proGender = findViewById(R.id.proGender);
        proAnx = findViewById(R.id.proAnx);
        proDep = findViewById(R.id.proDep);
        proOcd = findViewById(R.id.proOcd);
        proSch = findViewById(R.id.proSch);
        progressBar = findViewById(R.id.MainProgress);


        Bundle b = getIntent().getExtras();

        if(b!=null){
            ptId = String.valueOf(b.get("pid"));
        }

        //Toast.makeText(this, ptId, Toast.LENGTH_SHORT).show();
        reference= FirebaseDatabase.getInstance().getReference().child("Users");

        reference.child(ptId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                 age           = dataSnapshot.child("Age").            getValue().   toString();
                 gender        = dataSnapshot.child("Gender").         getValue().   toString();
                 name          =  dataSnapshot.child("Name").          getValue().   toString();
                 anxietyResult =  dataSnapshot.child("Anxiety Result").getValue().   toString();
                depressionResult =  dataSnapshot.child("Depression Result").getValue().   toString();
                ocdResult =  dataSnapshot.child("Obsessive compulsive disorder Result").getValue().          toString();
                schizoResult =  dataSnapshot.child("Schizophrenia Result").getValue().   toString();
               //Toast.makeText(patientDetails.this, name + gender + age + anxietyResult, Toast.LENGTH_SHORT).show();

                progressBar.setVisibility(View.INVISIBLE);

                proName.setText(name);
                proAge.setText(age);
                proGender.setText(gender);
                proAnx.setText(anxietyResult);
                proDep.setText(depressionResult);
                proOcd.setText(ocdResult);
                proSch.setText(schizoResult);

                proName.setVisibility(View.VISIBLE);
                proAge.setVisibility(View.VISIBLE);
                proGender.setVisibility(View.VISIBLE);
                proAnx.setVisibility(View.VISIBLE);
                proDep.setVisibility(View.VISIBLE);
                proOcd.setVisibility(View.VISIBLE);
                proSch.setVisibility(View.VISIBLE);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.messagemenu , menu);

        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item){

        if (item.getItemId() == R.id.msgs){

            Intent intent = new Intent(patientDetails.this , newMessages.class);
            intent.putExtra("msgK" , 0 );
            intent.putExtra("patId", ptId);
            intent.putExtra("patName", name);

            startActivity(intent);
        }

        return true;
    }
}
