package com.example.amad.mypsychiatrist;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class NotificationService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("Tagf",remoteMessage.toString());
        if (remoteMessage.getNotification()!= null){
            String title = remoteMessage.getNotification().getTitle();
            String body = remoteMessage.getNotification().getBody();
            //Log.d("Notification" , r);
            NotificationHelper mNotif = new NotificationHelper(this);
            mNotif.createNotification(title, body);
        }

        if (remoteMessage.getData().size() > 0){

            Log.d("Noti" , remoteMessage.getData().toString());
        }
    }
}
