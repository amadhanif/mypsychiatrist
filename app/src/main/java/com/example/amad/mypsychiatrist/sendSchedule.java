package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class sendSchedule extends AppCompatActivity {
  String ptId;
   String  name, anxiety, depression, ocd, schizophrenia;
    TextView nm , anxi, depr, obcd, schiz;
    EditText sTime;
    Button tSend;
    String time;


    DatabaseReference reference , databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_schedule);
        nm = findViewById(R.id.pName);
        anxi = findViewById(R.id.anx);
        depr = findViewById(R.id.dep);
        obcd = findViewById(R.id.obcd);
        schiz = findViewById(R.id.schizo);

        sTime = findViewById(R.id.sTime);
        tSend = findViewById(R.id.tSend);
        //TODO isy add krna hai sb me
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Send Schedule");
        Bundle b = getIntent().getExtras();

        if(b!=null){
            ptId = String.valueOf(b.get("pid"));
        }

        reference= FirebaseDatabase.getInstance().getReference().child("Users");

        reference.child(ptId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                name           =  dataSnapshot.    child("Name").                               getValue().   toString();
                anxiety        =  dataSnapshot.    child("Anxiety Result").                    getValue().   toString();
                depression     =  dataSnapshot.    child("Depression Result").                 getValue().   toString();
                ocd            =  dataSnapshot.    child("Obsessive compulsive disorder Result").getValue().   toString();
                schizophrenia  =  dataSnapshot.    child("Schizophrenia Result").               getValue().   toString();

                nm.setText(name);
                anxi.setText(anxiety);
                depr.setText(depression);
                obcd.setText(ocd);
                schiz.setText(schizophrenia);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        databaseReference= FirebaseDatabase.getInstance().getReference();
        tSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> hashMap = new HashMap<>();

                hashMap.put("Schedule", time);
                time = sTime.getText().toString();

                databaseReference = FirebaseDatabase.getInstance().getReference().child("Users").child(ptId);
                databaseReference.updateChildren(new HashMap(hashMap)).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){

                            Toast.makeText(sendSchedule.this, "Sechedule Sent", Toast.LENGTH_SHORT)
                                    .show();


                        }
                        else {
                            Toast.makeText(sendSchedule.this, "Error in Schedule Sending..", Toast.LENGTH_SHORT).show();
                        }

                    }
                });

                Toast.makeText(sendSchedule.this, name + anxiety ,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
