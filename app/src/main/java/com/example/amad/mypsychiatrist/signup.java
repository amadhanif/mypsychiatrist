package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.hbb20.CountryCodePicker;

import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class signup extends AppCompatActivity {
    EditText signUpEditTExt;
    CountryCodePicker ccp;
    LinearLayout passLinearLayout;
    LinearLayout phnLinearLAyout;
    RelativeLayout authRelativeLayout;
    EditText authEditText;
    Button authButton;
    public PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    public PhoneAuthProvider.ForceResendingToken resendToken;
    public FirebaseAuth mAuth;

    public String phoneVerificationId;
    Button signUp;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;



    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            Intent profileIntent= new Intent(signup.this, GenerateProfile.class);


                           // profileIntent.putExtra("phone_no",signUpEditTExt.getText().toString());
                            startActivity(profileIntent);
                            finish();
                        } else {
                            // Sign in failed, display a message and update the UI
                            Toast.makeText(signup.this, "There is some error!!!", Toast.LENGTH_LONG).show();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(signup.this, "Please enter valid verification code!!!", Toast.LENGTH_LONG).show();

                            }
                        }
                    }
                });
    }

   public void verifyNumber(View view){

        String inputCode= authEditText.getText().toString();

        //if (phoneVerificationId.equals("")){
            verifyPhoneNumber(phoneVerificationId, inputCode);

           // Toast.makeText(signup,"Please Wait a Moment",TO)
       Toast.makeText(signup.this, "Please Wait a Moment", Toast.LENGTH_SHORT).show();

   }

    private void verifyPhoneNumber(String phoneVerificationId, String inputCode) {

       PhoneAuthCredential credential= PhoneAuthProvider.getCredential(phoneVerificationId, inputCode);
       signInWithPhoneAuthCredential(credential);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        signUpEditTExt= findViewById(R.id.signupEditText);
        ccp =  findViewById(R.id.ccp);
        ccp.registerCarrierNumberEditText(signUpEditTExt);
        mAuth= FirebaseAuth.getInstance();
        signUp= findViewById(R.id.btn_signup);
        phnLinearLAyout= findViewById(R.id.phnLinearLayout);
        //passLinearLayout= findViewById(R.id.passLinearLayout);
        authRelativeLayout= findViewById(R.id.authRelativeLayout);
        authEditText= findViewById(R.id.authEditText);
        authButton= findViewById(R.id.authBtn);




        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String signUpPhoneNum= signUpEditTExt.getText().toString();
                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        signUpPhoneNum,        // Phone number to verify
                        60,                 // Timeout duration
                        TimeUnit.SECONDS,   // Unit of timeout
                        signup.this,// Activity (for callback binding)
                        mCallbacks
                );        // OnVerificationStateChangedCallbacks

                phnLinearLAyout.setVisibility(View.INVISIBLE);
                //passLinearLayout.setVisibility(View.INVISIBLE);
                signUp.setVisibility(View.INVISIBLE);
                authRelativeLayout.setVisibility(View.VISIBLE);

                Toast.makeText(signup.this, "A verification code is sent to your number", Toast.LENGTH_LONG).show();

            }

        });
        mCallbacks= new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

                signInWithPhoneAuthCredential(phoneAuthCredential);

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {

                Toast.makeText(signup.this, "Erorr occuring in verifying phone number!!!", Toast.LENGTH_LONG).show();

            }

            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {

                phoneVerificationId = verificationId;
                resendToken = token;

                // ...
            }

        };


    }

}
