package com.example.amad.mypsychiatrist;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ShowProfile extends AppCompatActivity {

   private FirebaseAuth mAuth;

   DatabaseReference reference;

   TextView userName, userAge, userGender , uName;

   ProgressBar mainProgress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_profile);

        userName = findViewById(R.id.UserEmailProfile);
        userAge  = findViewById(R.id.UserAgeProfile);
        userGender  = findViewById(R.id.gender);
        mainProgress = findViewById(R.id.MainProgress);
        uName = findViewById(R.id.UserNameProfile);

        mAuth=FirebaseAuth.getInstance();
        String current_user = mAuth.getCurrentUser().getUid();

        reference= FirebaseDatabase.getInstance().getReference().child("Users").child(current_user);

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String age = dataSnapshot.child("Age").getValue().toString();
                String gender= dataSnapshot.child("Gender").getValue().toString();
                String name=  dataSnapshot.child("Name").getValue().toString();
             //   String phone_no=  dataSnapshot.child("phone_no").getValue().toString();

               /* Toast.makeText(ShowProfile.this, "age"+ age + "gender" +gender + "name" +name , Toast.LENGTH_SHORT).show();*/

               mainProgress.setVisibility(View.INVISIBLE);

               userName.setText(name);
               userAge.setText(age);
               userGender.setText(gender);
               uName.setText(name);
               userName.setVisibility(View.VISIBLE);
                userAge.setVisibility(View.VISIBLE);
                userGender.setVisibility(View.VISIBLE);
                uName.setVisibility(View.VISIBLE);
                SharedPreferences.Editor editor = getSharedPreferences("ptNamePref", MODE_PRIVATE).edit();
                editor.putString("ptName", name);
                editor.apply();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }
}
