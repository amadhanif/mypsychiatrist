package com.example.amad.mypsychiatrist;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class OCDVideos extends AppCompatActivity {

    Toolbar mToolbar;
    WebView webView;
    TextView txt;
    String YouTubeVideoEmbedCode1 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/0EQL7UDg86U\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode2 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/ysldCUdGgxs\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode3 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/TcGsGF_us3w\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode4 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/Z6HSltxq0oo\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode5 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/Po-Gmd9byOQ\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode6 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/pSxs3YQBqiU\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode7 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/g0-oU51tdW8\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode8 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/hV2DOXhZ9qM\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode9 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/yJz9VKKFLZg\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode10 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/Rmzz2bvFrtw\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ocdvideos);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        webView = (WebView) findViewById(R.id.webView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar8);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString("CountryName"));
            if (mToolbar.getTitle().toString().equalsIgnoreCase("OCD – Introduction in Urdu")) {
                WebView();


            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Freedom from OCD - Binaural Beats Session - By Thomas Hall")) {
                WebView1();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Obsessive Compulsive Disorder (OCD) Frequency Healing with Affirmations/Subliminal/Energy")) {
                WebView2();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Obsessive Compulsive Disorder Treatment Frequency: OCD Relief Binaural Beats Sound Therapy")) {
                WebView4();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Freedom from OCD - Classical Music Subliminal Session - By Thomas Hall")) {
                WebView5();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Obsessive Compulsive Disorder - Rife Frequencies")) {
                WebView6();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("OCD Relief Release Unhealthy Mental and Physical Obsessions")) {
                WebView7();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("OCD Symptoms: Obsessive Compulsive Disorder Symptoms")) {
                WebView8();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Freedom from OCD - Waterfall Sounds Subliminal Session - By Thomas Hall")) {
                WebView9();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Natural Cure for OCD")) {
                WebView10();

            }


        }

    }

    private void WebView2() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode3, "text/html", "utf-8");
    }

    private void WebView1() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode2, "text/html", "utf-8");
    }

    public void WebView() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode1, "text/html", "utf-8");
    }


    private void WebView4() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode4, "text/html", "utf-8");
    }



    private void WebView5() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode5, "text/html", "utf-8");
    }

    private void WebView6() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode6, "text/html", "utf-8");
    }

    private void WebView7() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode7, "text/html", "utf-8");
    }


    private void WebView8() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode8, "text/html", "utf-8");
    }

    private void WebView9() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode9, "text/html", "utf-8");
    }

    private void WebView10() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode10, "text/html", "utf-8");
    }




}