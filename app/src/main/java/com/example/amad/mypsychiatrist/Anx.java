package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.amad.mypsychiatrist.R;

public class Anx extends AppCompatActivity {
    Toolbar toolbar;
    int[] images = {R.drawable.anxi1,R.drawable.anxi2,R.drawable.anxi3,R.drawable.anxi4,R.drawable.anxi5,R.drawable.anxi6,R.drawable.anxi7,R.drawable.anxi8,R.drawable.anxi9,R.drawable.anxi10};
    String[] lessons = {"Anxiety Free", "A True Perfectionist Mind & Dream", "Relax Your Brain", "Put Your Anxiety At Ease!!","Oddly Satisfying Video that Helps You Fall Asleep", "IF YOU GET SATISFIED YOU LOSE", "The Most Satisfying Videos In The World", "Relieve You of Your Worries","*IMPOSSIBLE* Try Not To Be Satisfied ", "Oddly Satisfying Video That Gives You Cheerful Mood"};
    ListView list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anx);
        list = (ListView) findViewById(R.id.list);
        toolbar = (Toolbar) findViewById(R.id.toolbar7);
        toolbar.setTitle("Videos");
        CustomArrayAdapter customlist = new CustomArrayAdapter(this, lessons, images);
        list.setAdapter(customlist);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                long pos=adapterView.getItemIdAtPosition(i);


                String title=String.valueOf(adapterView.getItemAtPosition(i));
                if (pos==0){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=tk1p3fD-mtg")));
                }
                else if(pos==1){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=YVEZ9KfxkWc")));
                }

                else if(pos==2){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=T1sa8uMvpp8")));
                }


                else if(pos==3){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=q4K6bu0Einc&t=58s")));
                }


                else if(pos==4){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=6H0r-bu4TWk")));
                }
                else if(pos==5){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=y8zMQKqCI4I")));
                }
                else if(pos==6){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=hCcEt3JsHEY")));
                }
                else if(pos==7){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("www.youtube.com/watch?v=sKDSnceMzIw&feature=youtu.be")));
                }
                else if(pos==8){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=e2BEAmUXKoA&t=1s")));
                }
                else if(pos==9 ){
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=PRBEh4hSOok")));
                }

            }

        });


    }


}