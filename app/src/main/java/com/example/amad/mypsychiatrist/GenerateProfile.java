package com.example.amad.mypsychiatrist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;

import adil.dev.lib.materialnumberpicker.dialog.GenderPickerDialog;

public class GenerateProfile extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    Spinner spinner;
    String phone_num;
    EditText nameEditText , ageEditText;
    Button generateProfileButton;
    FirebaseAuth mAuth;
    DatabaseReference reference;

    String gender;


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        //Toast.makeText(this, "Gender" + Integer.toString(i), Toast.LENGTH_SHORT).show();

        gender = adapterView.getItemAtPosition(i).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {


    }


    public void generateProfile(View view){

        String currentUser=mAuth.getCurrentUser().getUid();
        String token = FirebaseInstanceId.getInstance().getToken();
        reference = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser);
        String name= nameEditText.getText().toString();
        String age=  ageEditText.getText().toString();

        HashMap<String , String> hashMap = new HashMap<String, String>();
        hashMap.put("Name", name);
        hashMap.put("Age", age);
        hashMap.put("Gender", gender.toString());
        hashMap.put("uid", currentUser);
        hashMap.put("Anxiety Result" , "0.0");
        hashMap.put("Depression Result" , "0.0");
        hashMap.put("Obsessive compulsive disorder Result" , "0.0");
        hashMap.put("Schizophrenia Result" , "0.0");
        hashMap.put("Device Token" , token);
        hashMap.put("Schedule" , "0");
        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                    Toast.makeText(GenerateProfile.this, "Your Profile is Successfully Generated", Toast.LENGTH_SHORT)
                    .show();

                    Intent intent = new Intent(GenerateProfile.this,Main2Activity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(GenerateProfile.this, "Error in Data Storing..", Toast.LENGTH_SHORT).show();
                }
            }
        });
        /*reference.child("Name").setValue(name);
        reference.child("Age").setValue(age);
        reference.child("Gender").setValue(gender.toString());*/

      /*  Toast.makeText(GenerateProfile.this, "Your Profile is Successfully Generated", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(GenerateProfile.this,MainActivity.class);

        startActivity(intent);
*/



    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generate_profile);
        nameEditText= findViewById(R.id.nameEditText);
        ageEditText= findViewById(R.id.ageEditText);
       /* generateProfileButton= findViewById(R.id.generateProfileButton);*/

        mAuth = FirebaseAuth.getInstance();

        reference= FirebaseDatabase.getInstance().getReference();
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.gender_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

//        Intent pIntent= getIntent();
//       phone_num =  pIntent.getStringExtra("phone_no");

        SharedPreferences levels = getSharedPreferences("myPreff",MODE_PRIVATE);
        final SharedPreferences.Editor Pedit = levels.edit();


        Pedit.putString("level","1");

        Pedit.commit();



    }


    public void alreadyMember(View view) {

        Intent intent = new Intent(GenerateProfile.this , signIn.class);
        startActivity(intent);
    }
}
