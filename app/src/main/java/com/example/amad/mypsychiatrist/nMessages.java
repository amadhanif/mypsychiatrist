package com.example.amad.mypsychiatrist;

public class nMessages {
    public nMessages() {
    }

    public nMessages(String msg, String key, String sender) {
        this.msg = msg;
        this.key = key;
        this.sender = sender;
    }

    String msg;

    public String getMsg() {
        return msg;
    }

    public String getKey() {
        return key;
    }

    public String getSender() {
        return sender;
    }

    String key;
    String sender ;
}
