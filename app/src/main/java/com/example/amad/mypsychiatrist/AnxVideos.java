package com.example.amad.mypsychiatrist;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class AnxVideos extends AppCompatActivity {
    Toolbar mToolbar;
    WebView webView;
    TextView txt;
    String YouTubeVideoEmbedCode1 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/tk1p3fD-mtg\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode2 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/YVEZ9KfxkWc\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode3 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/T1sa8uMvpp8\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode4 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/q4K6bu0Einc&t\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode5 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/6H0r-bu4TWk\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode6 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/hCcEt3JsHEY\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode7 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/sKDSnceMzIw&feature\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode8 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/e2BEAmUXKoA&t\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode9 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/PRBEh4hSOok\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode10 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/y8zMQKqCI4I\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anx_videos);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        webView = (WebView) findViewById(R.id.webView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar8);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString("CountryName"));
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Anxiety Free")) {
                WebView();


            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("A True Perfectionist Mind & Dream")) {
                WebView1();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Relax Your Brain")) {
                WebView2();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Put Your Anxiety At Ease!!")) {
                WebView4();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Oddly Satisfying Video that Helps You Fall Asleep")) {
                WebView5();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("IF YOU GET SATISFIED YOU LOSE")) {
                WebView6();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("The Most Satisfying Videos In The World")) {
                WebView7();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Relieve You of Your Worries")) {
                WebView8();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("*IMPOSSIBLE* Try Not To Be Satisfied ")) {
                WebView9();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Oddly Satisfying Video That Gives You Cheerful Mood")) {
                WebView10();

            }


        }

    }

    private void WebView2() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode3, "text/html", "utf-8");
    }

    private void WebView1() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode2, "text/html", "utf-8");
    }

    public void WebView() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode1, "text/html", "utf-8");
    }


    private void WebView4() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode4, "text/html", "utf-8");
    }



    private void WebView5() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode5, "text/html", "utf-8");
    }

    private void WebView6() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode6, "text/html", "utf-8");
    }

    private void WebView7() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode7, "text/html", "utf-8");
    }


    private void WebView8() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode8, "text/html", "utf-8");
    }

    private void WebView9() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode9, "text/html", "utf-8");
    }

    private void WebView10() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode10, "text/html", "utf-8");
    }




}