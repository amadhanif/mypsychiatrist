package com.example.amad.mypsychiatrist;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class SchVideos extends AppCompatActivity {

    Toolbar mToolbar;
    WebView webView;
    TextView txt;
    String YouTubeVideoEmbedCode1 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/Rg8Hcs4uX5w\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode2 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/-aZqbaA-qzUU\" frameborder=\"0\" allowfullscreen></iframe></body></html>";


   // String YouTubeVideoEmbedCode2 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/-aZqbaA-qzUU\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode3 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/AlkbK9viSsc\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode4 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/ybf5ov2qBVA\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode5 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/nZhX_vGDXJc\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode6 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/jbfP6j5JBh0\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode7 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/KAgUUFjPp2w\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode8 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/Zyh-1Y-0rzU\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode9 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/fuCSegWKfQA\" frameborder=\"0\" allowfullscreen></iframe></body></html>";
    String YouTubeVideoEmbedCode10 = "<html><body><iframe width=\"350\" height=\"350\" src=\"https://www.youtube.com/embed/8eC8xAJR0p8\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sch_videos);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        webView = (WebView) findViewById(R.id.webView);
        mToolbar = (Toolbar) findViewById(R.id.toolbar8);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString("CountryName"));
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Cure for Schizophrenia Hypnosis")) {
                WebView();


            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Cure to Schizophrenia")) {
                WebView99();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Schizophrenia – Introduction in Urdu")) {
                WebView2();

            } else if (mToolbar.getTitle().toString().equalsIgnoreCase("Healing Code for Schizophrenia")) {
                WebView4();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Treat Schizophrenia Subliminal Extremely Powerful and Very Fast Results")) {
                WebView5();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Music Therapy for Schizophrenia Hypnosis")) {
                WebView6();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Get Rid of Schizophrenia Subliminal")) {
                WebView7();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Schizophrenia – Musical Simulation")) {
                WebView8();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Higher Self voice – Schizophrenia Healing")) {
                WebView9();

            }
            else if (mToolbar.getTitle().toString().equalsIgnoreCase("Natural Cure for Schizophrenia")) {
                WebView10();

            }


        }

    }

    private void WebView99() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode2, "text/html", "utf-8");
    }

    private void WebView2() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode3, "text/html", "utf-8");
    }

   /* private void WebView1() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode2, "text/html", "utf-8");
    }*/

    public void WebView() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode1, "text/html", "utf-8");
    }


    private void WebView4() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode4, "text/html", "utf-8");
    }



    private void WebView5() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode5, "text/html", "utf-8");
    }

    private void WebView6() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode6, "text/html", "utf-8");
    }

    private void WebView7() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode7, "text/html", "utf-8");
    }


    private void WebView8() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode8, "text/html", "utf-8");
    }

    private void WebView9() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode9, "text/html", "utf-8");
    }

    private void WebView10() {
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadData(YouTubeVideoEmbedCode10, "text/html", "utf-8");
    }




}