import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
exports.notifAdmin = functions.database.ref('/request/{reqKey}').onCreate(

  (s)=>{

  	const payLoad = {
  		notification:{
  			title:'New Appointment Request',
  			body : `Appointment from ${s.val().Name}`
  		}
  	};
  	//const text = snapshot.val().text;
  	return admin.database().ref('/admin/token').
  	once('value').then(
  		(myToken) => {
  			if (myToken.val()) {
  				return admin.messaging().sendToDevice(myToken.val() , payLoad);
  				console.log(`notification sent`);
  				return 1;
  			}
  			return {};
  		}
  		);
  	
  
 });
